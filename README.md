# Challenge 3: Website Deployment

Welcome to Challenge 3.

This project was bootstrapped with [Create Next App](https://github.com/segmentio/create-next-app).

## Task 1 

Given this project deploy it to AWS in an automated and reproducible fashion. The website should be reachable from all over the world.

Answer: This is accessable from all over world

## Task 2 

Restrict access to the site by using mechanisms that can be adapted programmatically.

Answer: Using IAM resources in Terraform we can Restrict the access

## Task 3 

Deploy the site using at least 2 technology stacks. Make sure that both types of deployment can be reproduced in an automated fashion.
MEAN, 

Answer: Used GITLAB for CI/CD, Terraform for EC2 Creation on AWS, Docker for hosting the Website

## Task 4 


What issues can you identify in the given site? What types of improvements would you suggest?

Answer: Could see 

Breach Report
Here are some current data breaches for you
      Doomworld
Breached on: 2022-10-12
Accounts breached: 34478

Get Revenge On Your Ex
Breached on: 2022-09-09
Accounts breached: 79195

Need to look from Data Security perspective and DevSecOps tooling.
