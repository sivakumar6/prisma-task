#output "instance-id" {
#  description = "The EC2 instance ID"
#  value       = "${aws_instance.instance.id}"
#}

output "instance-public-ip" {
  description = "The EC2 instance public IP"
  value       = "${aws_instance.instance.public_ip}"
}
